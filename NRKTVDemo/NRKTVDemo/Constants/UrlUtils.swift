import Foundation

struct BaseURLs {
    static let headliner = "https://psapi.nrk.no/tv/headliners/"
    static let program = "https://psapi.nrk.no/playback/manifest/program/"
}

struct UrlUtils {
    static func headlinersURL(age: String? = nil) -> URLComponents {
        let baseURL = BaseURLs.headliner + "default"
        var components = URLComponents(string: baseURL)
        components!.queryItems = [
            URLQueryItem(name: "age", value: age),
        ]
        
        return components!
    }
    
    static func programURL(id: String) -> URLComponents {
        let baseURL = BaseURLs.program + id
        let components = URLComponents(string: baseURL)
    
        return components!
    }
}
