import SwiftUI

extension Color {
    static let ui = Color.UI()
    
    struct UI {
        let nrkblue = Color("NRKBlue")
        let sliderblue = Color("Slider")
    }
}
