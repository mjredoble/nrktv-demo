import CachedAsyncImage
import SwiftUI

struct RowFeatureView: View {
    @ObservedObject var viewModel: MainViewModel
    var leftHeadliner: Headliner
    var rightHeadliner: Headliner
    
    var body: some View {
        HStack(alignment: .center) {
            NavigationLink(destination: VideoPlayerView(viewModel: viewModel, headliner: leftHeadliner), label: {
                VStack(spacing: 0) {
                    CachedAsyncImage(url: URL(string: leftHeadliner.images[0].uri), content: { image in
                        image
                            .resizable()
                            .frame(height: 242)
                    }, placeholder: {
                        ProgressView()
                            .padding()
                    })
                }
                .cornerRadius(5)
            })
            
            NavigationLink(destination: VideoPlayerView(viewModel: viewModel, headliner: rightHeadliner), label: {
                VStack(spacing: 0) {
                    CachedAsyncImage(url: URL(string: rightHeadliner.images[0].uri), content: { image in
                        image
                            .resizable()
                            .frame(height: 242)
                    }, placeholder: {
                        ProgressView()
                            .padding()
                    })
                }
                .cornerRadius(5)
            })
        }
        .padding(5)
    }
}
