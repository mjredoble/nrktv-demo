import SwiftUI
import AVKit

struct VideoPlayerView: View {
    @ObservedObject var viewModel: MainViewModel
    var headliner: Headliner

    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @State private var player = AVPlayer()
    
    init(viewModel: MainViewModel, headliner: Headliner) {
        self.viewModel = viewModel
        self.headliner = headliner
        self.setupView()
    }
    
    private func setupView() {
        UINavigationBar.appearance().largeTitleTextAttributes = [.font : UIFont(name: "Georgia-Bold", size: 30)!, .foregroundColor: UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = [.font : UIFont(name: "Georgia-Bold", size: 20)!, .foregroundColor: UIColor.white]
        UINavigationBar.appearance().backgroundColor = UIColor(Color.nrkBlue)
        UINavigationBar.appearance().barTintColor = UIColor(Color.nrkBlue)
    }
    
    var body: some View {
        NavigationStack {
            ZStack(alignment: .topLeading) {
                VideoPlayer(player: player)
                    .edgesIgnoringSafeArea(.all)
                    .navigationBarBackButtonHidden()
                    .onDisappear {
                        player.pause()
                    }
                
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Image(systemName: "arrow.left.circle.fill")
                        .resizable()
                        .frame(width: 30, height: 30)
                        .foregroundColor(.white)
                        .padding(.leading, 20)
                        .padding(.top, 50)
                }
            }
            .task {
                do {
                    try await viewModel.fetchPlaybackData(selectedId: headliner.contentId)
                    let url = URL(string: viewModel.selectedVideoURL)!
                    player = AVPlayer(url: url)
                    player.play()
                } catch {
                    print("ERROR: \(error)")
                    self.presentationMode.wrappedValue.dismiss()
                }
            }
        }
    }
}
