import AVFoundation
import _AVKit_SwiftUI
import SwiftUI
import CachedAsyncImage

struct MainView: View {
    @ObservedObject var viewModel = MainViewModel()
    
    init() {
        UINavigationBar.appearance().largeTitleTextAttributes = [.font : UIFont(name: "Georgia-Bold", size: 30)!, .foregroundColor: UIColor.white]
        UINavigationBar.appearance().titleTextAttributes = [.font : UIFont(name: "Georgia-Bold", size: 20)!, .foregroundColor: UIColor.white]
        UINavigationBar.appearance().backgroundColor = UIColor(Color.nrkBlue)
        UINavigationBar.appearance().barTintColor = UIColor(Color.nrkBlue)
    }
    
    var body: some View {
        NavigationStack {
            ZStack(alignment: .bottom) {
                ScrollView {
                    VStack(alignment: .leading, spacing: 10) {
                        if viewModel.headliners.count > 0 {
                            FeatureView(viewModel: viewModel, headliner: viewModel.headliners[2])

                            RowFeatureView(viewModel: viewModel, leftHeadliner: viewModel.headliners[1], rightHeadliner: viewModel.headliners[0])
                            
                            FeatureView(viewModel: viewModel, headliner: viewModel.headliners[3])
                                
                            RowFeatureView(viewModel: viewModel, leftHeadliner: viewModel.headliners[4], rightHeadliner: viewModel.headliners[5])
                        }
                    }
                    .frame(maxWidth: .infinity,
                           maxHeight: .infinity,
                           alignment: .leading)
                    .padding()
                }
                
                
                HStack(alignment: .center) {
                    Text("0")
                        .foregroundStyle(.white)
                        .fontWeight(.bold)
                        .padding(.leading)
                    
                    VStack {
                        Slider(value: $viewModel.selectedAge, in: 0...18, step: 1)
                            .padding(10)
                            .onChange(of: viewModel.selectedAge) { newValue in
                                Task {
                                    do {
                                        try await viewModel.fetchHeadliners()
                                    } catch {
                                        print("ERROR: \(error)")
                                    }
                                }
                            }
                    }
                    
                    Text("18+")
                        .foregroundStyle(.white)
                        .fontWeight(.bold)
                        .padding(.trailing)
                }
                .background(Color.ui.sliderblue)
                .cornerRadius(5)
                .padding(.horizontal)
            }
            .background(Color.ui.nrkblue)
            .task {
                do {
                    try await viewModel.fetchHeadliners()
                } catch {
                    print("ERROR: \(error)")
                }
            }
            .navigationTitle($viewModel.title)
        }
    }
}

#Preview {
    MainView()
}

struct FeatureView: View {
    @ObservedObject var viewModel: MainViewModel
    var headliner: Headliner
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            ZStack(alignment: .bottomLeading) {
                CachedAsyncImage(url: URL(string: headliner.images[1].uri), content: { image in
                    image
                        .resizable()
                        .scaledToFill()
                        .frame(width: 335, height: 335)
                }, placeholder: {
                    ProgressView()
                        .padding()
                })
                
                if headliner.type == .program {
                    NavigationLink(destination: VideoPlayerView(viewModel: viewModel, headliner: headliner), label: {
                        HStack {
                            Image("playBtn")
                                .resizable()
                                .frame(width: 60, height: 60)
                            
                            VStack(alignment: .leading) {
                                Text(headliner.title)
                                    .fontWeight(.bold)
                                    .foregroundStyle(.white)
                                Text(headliner.subTitle)
                                    .fontWeight(.regular)
                                    .foregroundStyle(.white)
                                    .lineLimit(2)
                            }
                        }
                        .padding(10)
                    })
                }
            }
            .frame(height: 335)
        }
        .cornerRadius(5)
    }
}
