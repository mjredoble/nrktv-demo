import SwiftUI

@MainActor
final class MainViewModel: ObservableObject {
    @Published var headliners: [Headliner] = []
    @Published var status = APIRequestStatus.done
    @Published var title = ""
    @Published var selectedVideoURL = ""
    @Published var selectedAge: Double = 0
    
    func fetchHeadliners() async throws {
        let components = UrlUtils.headlinersURL(age: "\(Int(selectedAge))")
        guard let url = components.url else {
            throw ServiceError.badUrl
        }
        
        status = .inprogress
        
        do {
            let result = try await HTTPClient().request(url: url, method: HTTPMethod.GET)
            let dto = try JSONDecoder().decode(HeadlinersDTO.self, from: result.body)
            self.title = dto.title
            
            self.headliners.removeAll()
            self.headliners.append(contentsOf: dto.headliners)
            status = .done
        } catch {
            status = .done
            throw ServiceError.badResponse
        }
    }
    
    func fetchPlaybackData(selectedId: String) async throws {
        let components = UrlUtils.programURL(id: selectedId)
        guard let url = components.url else {
            throw ServiceError.badUrl
        }
        
        status = .inprogress
        
        do {
            let result = try await HTTPClient().request(url: url, method: HTTPMethod.GET)
            let dto = try JSONDecoder().decode(PlaybackDTO.self, from: result.body)
            print(dto.playable.assets.first!.url)
            self.selectedVideoURL = dto.playable.assets.first!.url
            status = .done
        } catch {
            status = .done
            throw ServiceError.badResponse
        }
    }
}
