import SwiftUI

struct AgeSelectionView: View {
    @State private var age: Double = 0
    
    var callback: (Int) -> ()
    
    var body: some View {
        HStack(alignment: .center) {
            Text("0")
                .foregroundStyle(.white)
                .fontWeight(.bold)
                .padding(.leading)
            
            VStack {
                Slider(value: $age, in: 0...18, step: 1)
                    .padding(10)
                    .onChange(of: age) { newValue in
                        callback(Int(newValue))
                    }
            }
            
            Text("18+")
                .foregroundStyle(.white)
                .fontWeight(.bold)
                .padding(.trailing)
        }
        .background(Color.ui.sliderblue)
        .cornerRadius(5)
        .padding(.horizontal)
    }
}
