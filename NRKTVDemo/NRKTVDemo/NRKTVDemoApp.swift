import SwiftUI

@main
struct NRKTVDemoApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
