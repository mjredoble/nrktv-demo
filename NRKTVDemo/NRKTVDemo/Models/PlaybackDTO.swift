import Foundation

struct PlaybackDTO: Codable {
    let links: Links
    let id: String
    let playability: String
    let streamingMode: String
    let availability: Availability
    let statistics: Statistics
    let playable: Playable
    let displayAspectRatio: String
    let sourceMedium: String

    enum CodingKeys: String, CodingKey {
        case links = "_links"
        case id, playability, streamingMode, availability, statistics, playable, displayAspectRatio, sourceMedium
    }
}

struct Href: Codable {
    let href: String
}

struct Metadata: Codable {
    let href: String
    let name: String
}

struct Availability: Codable {
    let information: String
    let isGeoBlocked: Bool
    let onDemand: OnDemand?
    let live: String?
    let externalEmbeddingAllowed: Bool
}

struct OnDemand: Codable {
    let from: String
    let to: String
    let hasRightsNow: Bool
}

struct Statistics: Codable {
    let scores: Scores
    let ga: Ga
    let conviva: String?
    let luna: Luna
    let qualityOfExperience: QualityOfExperience
    let snowplow: Snowplow
}

struct Scores: Codable {
    let springStreamSite: String
    let springStreamStream: String
    let springStreamContentType: String
    let springStreamProgramId: String
    let springStreamDuration: Int
}

struct Ga: Codable {
    let dimension1, dimension2, dimension3, dimension4, dimension5: String
    let dimension10, dimension21, dimension22, dimension23, dimension25: String
    let dimension26, dimension29, dimension36: String
}

struct Luna: Codable {
    let config: Config
    let data: LunaData
}

struct Config: Codable {
    let beacon: String
}

struct LunaData: Codable {
    let title, device, playerId, deliveryType: String
    let playerInfo, cdnName: String
}

struct QualityOfExperience: Codable {
    let clientName, cdnName, streamingFormat, segmentLength: String
    let assetType, correlationId: String
}

struct Snowplow: Codable {
    let source: String
}

struct Playable: Codable {
    let endSequenceStartTime: String?
    let duration: String
    let assets: [Asset]
    let liveBuffer: String?
    let subtitles: [Subtitle]
    let thumbnails: [String]
    let thumbnail2D: Thumbnail2D
}

struct Asset: Codable {
    let url: String
    let format: String
    let mimeType: String
    let encrypted: Bool
}

struct Subtitle: Codable {
    let type: String
    let language: String
    let label: String
    let defaultOn: Bool
    let webVtt: String
}

struct Thumbnail2D: Codable {
    let url: String
}
