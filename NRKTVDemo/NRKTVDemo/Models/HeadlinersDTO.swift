import Foundation

enum MediaType: String, Codable {
    case series
    case program
}
                        
struct HeadlinersDTO: Codable {
    let title: String
    let headliners: [Headliner]
    let links: Links
    
    private enum CodingKeys: String, CodingKey {
        case title
        case headliners
        case links = "_links"
    }
}

struct Headliner: Codable {
    let contentId: String
    let title: String
    let type: MediaType
    let subTitle: String
    let images: [ImageDTO]
    let links: HeadlinerLinks
    
    private enum CodingKeys: String, CodingKey {
        case contentId, title, type, subTitle, images
        case links = "_links"
    }
}

struct ImageDTO: Codable {
    let uri: String
    let width: Int
}

struct Links: Codable {
    let selfLink: Link
    
    private enum CodingKeys: String, CodingKey {
        case selfLink = "self"
    }
}

struct HeadlinerLinks: Codable {
    let selfLink: Link
    let fargerik: Link
    let series: Link?
    let seriesPage: Link?
    let trailer: Trailer?
    let programs: Link?
    let playback: Link?
    
    private enum CodingKeys: String, CodingKey {
        case selfLink = "self"
        case fargerik, series, seriesPage = "seriespage", trailer
        case programs, playback
    }
}

struct Link: Codable {
    let href: String
}

struct Trailer: Codable {
    let name: String
    let href: String
}
